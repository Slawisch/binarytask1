﻿using System;
using System.Collections.Generic;
using System.Linq;
using BinaryTask1.Models;

namespace BinaryTask1.Services
{
    class LectureService
    {
        private HttpService httpService;
        private List<Project> _projects;
        private List<Task> _tasks;
        private List<Team> _teams;
        private List<User> _users;
        
        private IEnumerable<ProjectInfo> _projectInfos;

        public LectureService(HttpService service)
        {
            httpService = service;
            GetData();
        }

        private IEnumerable<ProjectInfo> GetProjectInfos()
        {
            var result = from p in _projects
                join t in _teams on p.TeamId equals t.Id
                join u in _users on p.AuthorId equals u.Id
                select new ProjectInfo()
                {
                    Id = p.Id,
                    TeamId = p.TeamId,
                    AuthorId = p.AuthorId,
                    Name = p.Name,
                    Description = p.Description,
                    Deadline = p.Deadline,
                    CreatedAt = p.CreatedAt,
                    Team = t,
                    Author = u,
                    Tasks = (from task in _tasks
                        join performer in _users on task.PerformerId equals performer.Id
                        where task.ProjectId == p.Id
                        select new TaskInfo()
                        {
                            Id = task.Id,
                            PerformerId = task.PerformerId,
                            ProjectId = task.ProjectId,
                            Performer = performer,
                            Description = task.Description,
                            CreatedAt = task.CreatedAt,
                            FinishedAt = task.FinishedAt,
                            Name = task.Name,
                            State = task.State
                        })
                };

            return result;
        }

        private void GetData()
        {
            httpService.GetData();
            _projects = httpService.Projects;
            _tasks = httpService.Tasks;
            _teams = httpService.Teams;
            _users = httpService.Users;

            _projectInfos = GetProjectInfos();

        }

        public Dictionary<ProjectInfo, int> GetProjectTaskCountByUser(int id)
        {
            Dictionary<ProjectInfo, int> result = new Dictionary<ProjectInfo, int>();
            _projectInfos.Where(p => p.Author.Id == id).ToList()
                .ForEach(p => result.Add(p, p.Tasks.Count()));

            return result;
        }

        public IEnumerable<TaskInfo> GetTasksByUserLess45(int id)
        {
            var result = _projectInfos.SelectMany(p => p.Tasks)
                .Where(t =>
                    t.Name != null &&
                    t.Performer.Id == id &&
                    t.Name.Length < 45).ToList();
            return result;
        }

        public IEnumerable<(int, string)> GetTasksByUserDone(int id)
        {
            var result = _projectInfos.SelectMany(p => p.Tasks)
                .Where(t =>
                    t.Performer.Id == id &&
                    t.FinishedAt.HasValue &&
                    t.FinishedAt.Value.Year == DateTime.Now.Year).Select(t => (t.Id, t.Name));
            return result;
        }

        public IEnumerable<(int, string, IEnumerable<User>)> GetTeamsWithUsersOlder10()
        {
            var result = _projectInfos.Select(p => p.Team).Distinct()
                .GroupJoin(
                    _projectInfos.SelectMany(p => p.Tasks)
                        .Select(taskInfo => taskInfo.Performer)
                        .Union(_projectInfos
                            .Select(pr => pr.Author)).Distinct(), 
                    team => team.Id, 
                    user => user.TeamId, 
                    (team, user) => (team.Id, team.Name, user))
                .Where(item => item.user.All(us => us.BirthDay.Year < DateTime.Now.Year - 10));

            return result;
        }

        public IEnumerable<IGrouping<User, (User, TaskInfo)>> GetUsersWithTasks()
        {
            var result = _projectInfos.SelectMany(p => p.Tasks)
                .Select(taskInfo => taskInfo.Performer)
                .Union(_projectInfos
                    .Select(pr => pr.Author)).Distinct()
                .Join(_projectInfos.SelectMany(p => p.Tasks),
                    u => u.Id,
                    t => t.PerformerId,
                    (u, t) => (u, t))
                .GroupBy(ut => ut.u)
                .SelectMany(g => g.OrderByDescending(grp => (grp.t.Name ?? "").Length))
                .GroupBy(gr => gr.u)
                .OrderBy(g => g.Key.FirstName);

            return result;
        }

        public UserStruct GetUserStruct(int id)
        {
            var result = _projectInfos.Select(_ => new UserStruct(){
                User = (_projectInfos
                    .SelectMany(p => p.Tasks)
                    .Select(taskInfo => taskInfo.Performer).Union(_projectInfos
                        .Select(pr => pr.Author)).Distinct().FirstOrDefault(u => u.Id == id)),
                LastProject = (_projectInfos.Where(p => p.AuthorId == id)
                    .OrderBy(p => p.CreatedAt).Last()),
                ProjectTaskCount = (_projectInfos.SelectMany(p => p.Tasks)
                    .Where(t => t.ProjectId == 
                                (_projectInfos.Where(p => p.AuthorId == id)
                                    .OrderBy(p => p.CreatedAt).Last()).Id)).Count(),
                UnfinishedTaskCount = (_projectInfos.SelectMany(p => p.Tasks)
                    .Where(t =>
                        t.Name != null &&
                        t.Performer.Id == id &&
                        t.FinishedAt == null)).Count(),
                LongestTask = _projectInfos.SelectMany(p => p.Tasks)
                    .Where(t =>
                        t.Name != null &&
                        t.Performer.Id == id).OrderBy(t => (t.FinishedAt ?? DateTime.Now) - t.CreatedAt).Last()
            }).FirstOrDefault();

            return result;
        }

        public IEnumerable<ProjectStruct> GetProjectStruct()
        {
            var result = _projectInfos.Select(p => new ProjectStruct()
            {
                Project = p,
                LongestTask = (_projectInfos.SelectMany(pr => pr.Tasks)
                        .Where(t => t.ProjectId == p.Id))
                    .OrderBy(t => (t.Description ?? "").Length).LastOrDefault(),
                ShortestTask = (_projectInfos.SelectMany(pr => pr.Tasks)
                        .Where(t => t.ProjectId == p.Id))
                    .OrderBy(t => (t.Name ?? "").Length).FirstOrDefault(),
                UserCount = (_projectInfos
                    .SelectMany(pi => pi.Tasks)
                    .Select(taskInfo => taskInfo.Performer)
                    .Union(_projectInfos
                        .Select(pr => pr.Author))
                    .Where(u =>
                        (u.TeamId ?? -1) == p.Team.Id &&
                        ((p.Tasks.Count() < 3) || ((p.Description ?? "").Length > 20))
                        )
                    .Distinct().Count())
            }).ToList();

            return result;
        }
    }
}