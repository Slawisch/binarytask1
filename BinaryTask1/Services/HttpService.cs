﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using BinaryTask1.Models;
using Newtonsoft.Json;

namespace BinaryTask1.Services
{
    class HttpService
    {
        private HttpClient _httpClient;
        private Uri _baseUri = new("https://bsa21.azurewebsites.net/");

        internal List<Project> Projects;
        internal List<Task> Tasks;
        internal List<Team> Teams;
        internal List<User> Users;

        public HttpService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public void GetData()
        {
            var userTask = _httpClient.GetAsync(_baseUri + $"api/Users/");
            var projectTask = _httpClient.GetAsync(_baseUri + $"api/Projects/");
            var tasksTask = _httpClient.GetAsync(_baseUri + $"api/Tasks/");
            var teamsTask = _httpClient.GetAsync(_baseUri + $"api/Teams/");

            var usersJson = userTask.Result.Content.ReadAsStringAsync().Result;
            var tasksJson = tasksTask.Result.Content.ReadAsStringAsync().Result;
            var teamsJson = teamsTask.Result.Content.ReadAsStringAsync().Result;
            var projectsJson = projectTask.Result.Content.ReadAsStringAsync().Result;

            try
            {
                Projects = JsonConvert.DeserializeObject<List<Project>>(projectsJson);
                Tasks = JsonConvert.DeserializeObject<List<Task>>(tasksJson);
                Teams = JsonConvert.DeserializeObject<List<Team>>(teamsJson);
                Users = JsonConvert.DeserializeObject<List<User>>(usersJson);
            }
            catch
            {
                throw new Exception("Can't get response.");
            }
            
        }
    }
}
