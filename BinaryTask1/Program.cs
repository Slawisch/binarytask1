﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using BinaryTask1.Services;
using Newtonsoft.Json;

namespace BinaryTask1
{
    class Program
    {
        private static readonly HttpClient httpClient = new HttpClient();
        
        
        static void Main(string[] args)
        {
            HttpService httpService = new HttpService(httpClient);
            LectureService lectureService = new LectureService(httpService);

            int userIdQuery1 = 31;
            int userIdQuery2 = 107;
            int userIdQuery3 = 108;
            int userIdQuery6 = 36;

            var res = lectureService.GetProjectTaskCountByUser(userIdQuery1);
            var res2 = lectureService.GetTasksByUserLess45(userIdQuery2);
            var res3 = lectureService.GetTasksByUserDone(userIdQuery3);
            var res4 = lectureService.GetTeamsWithUsersOlder10();
            var res5 = lectureService.GetUsersWithTasks();
            var res6 = lectureService.GetUserStruct(userIdQuery6);
            var res7 = lectureService.GetProjectStruct();


            Console.WriteLine($"1st query (id = {userIdQuery1}):");
            Console.ForegroundColor = ConsoleColor.Yellow;
            foreach (var item in res)
            {
                Console.WriteLine(item.Key + "\t" + item.Value);
            }

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine($"\n2nd query (id = {userIdQuery2}):");
            Console.ForegroundColor = ConsoleColor.Blue;
            foreach (var item in res2)
            {
                Console.WriteLine(item);
            }

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine($"\n3rd query (id = {userIdQuery3}):");
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            foreach (var item in res3)
            {
                Console.WriteLine(item.Item1 + "\t" + item.Item2);
            }

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine($"\n4th query :");
            Console.ForegroundColor = ConsoleColor.Magenta;
            foreach (var item in res4)
            {
                Console.WriteLine(item.Item1 + "\t" + item.Item2 + "\n");
                foreach (var user in item.Item3)
                {
                    Console.WriteLine("\t"+user);
                }
                Console.WriteLine();
            }

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("\n5th query:");
            Console.ForegroundColor = ConsoleColor.Green;
            foreach (var item in res5)
            {
                Console.WriteLine(item.Key);
                foreach (var userTask in item)
                {
                    Console.WriteLine("\t"+userTask.Item2);   
                }
            }

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine($"\n6th query (id = {userIdQuery6}):");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(res6);

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine($"\n7th query:");
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            foreach (var item in res7)
            {
                Console.WriteLine(item+"\n");
            }

        }

       
    }
}
