﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTask1.Models
{
    public class ProjectInfo : Project
    {
        public Team Team { get; set; }
        public User Author { get; set; }
        public IEnumerable<TaskInfo> Tasks;
    }
}
