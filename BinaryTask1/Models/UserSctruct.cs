﻿namespace BinaryTask1.Models
{
    public struct UserStruct
    {
        public User User { get; set; }
        public ProjectInfo LastProject { get; set; }
        public int ProjectTaskCount { get; set; }
        public int UnfinishedTaskCount { get; set; }
        public TaskInfo LongestTask { get; set; }

        public override string ToString()
        {
            return $"{User}\n{LastProject}\n{ProjectTaskCount}\n{UnfinishedTaskCount}\n{LongestTask}";
        }
    }
}