﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTask1.Models
{
    public class TaskInfo : Task
    {
        public User Performer { get; set; }
    }
}
